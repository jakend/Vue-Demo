var webpack = require("webpack");
module.exports = {
  watch: true, //监听变化自动编译
  entry: {
    index: "./src/main.js"
  },
  output: {
    path: "./dist/",
    filename: "[name].min.js",
    publicPath: "./dist/"
  },
  module: {
    loaders: [
      {
        test: /\.vue$/, //解析vue模板
        loader: "vue-loader"
      },
      {
        test: /\.js$/, //js转换
        exclude: /(node_modules)/,
        loader: "babel-loader",
        query: {
          presets: ["es2015"]
        }
      },
      {
        test: /\.css$/, //css转换
        loader: "vue-style!css"
      }
    ]
  },
  vue: {
    loaders: {
      css: "vue-style!css"
    }
  }
};
